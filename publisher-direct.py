import pika
from faker import Faker

connection_parameters = pika.ConnectionParameters(host='localhost')
fake = Faker('iw_IL')

with pika.BlockingConnection(connection_parameters) as connection:
    channel = connection.channel()
    channel.exchange_declare(exchange='MyDirectExchange', exchange_type='direct')
    channel.queue_declare(queue='MyQueue')
    


    for x in range(1000):      
        person = fake.name() 
        location = fake.city() 
        message = f"{person} is in city {location}"
        channel.basic_publish(exchange='MyDirectExchange',routing_key=location ,body=message)


