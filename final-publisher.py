import pika
from faker import Faker

connection_parameters = pika.ConnectionParameters(host='localhost')
fake = Faker('iw_IL')

with pika.BlockingConnection(connection_parameters) as connection:
    channel = connection.channel()
    channel.exchange_declare(exchange='MyTopicExchange', exchange_type='topic')
  
    for x in range(1000):      
        person = fake.name()
        phone = fake.phone_number()
        mylocation = fake.city() 
        phone1 = fake.phone_number()
        phone2 = fake.phone_number()

        phoneCall = f"phoneCall.{phone1}.{phone2}"
        #message = f"Number {fake.phone_number()} messaged {fake.phone_number()}: {fake.paragraph(nb_sentences=2)}"
        location = f"{fake.phone_number()} is in city {fake.city()}"
        message = f"PhoneCall: Number {phone1} called {phone2}"
        channel.basic_publish(exchange='MyTopicExchange',routing_key=phoneCall ,body=message)

