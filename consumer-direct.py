import pika
from bidi.algorithm import get_display

def callback(ch, method, properties, body):
    print(f"Got message: " + get_display(body.decode('utf-8')))


connection_parameters = pika.ConnectionParameters(host='localhost')
with pika.BlockingConnection(connection_parameters) as connection:
    channel = connection.channel()
    channel.exchange_declare(exchange='MyDirectExchange', exchange_type='direct')
    channel.queue_declare(queue='MyQueue')
    channel.queue_bind(exchange='MyDirectExchange', queue='MyQueue',routing_key='אור הנר')
    channel.basic_consume(queue='MyQueue',on_message_callback=callback,auto_ack=True)
    channel.start_consuming()


