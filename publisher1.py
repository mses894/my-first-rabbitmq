import pika
from faker import Faker

connection_parameters = pika.ConnectionParameters(host='localhost')
fake = Faker('iw_IL')

with pika.BlockingConnection(connection_parameters) as connection:
    channel = connection.channel()
    channel.exchange_declare(exchange='MyExchange', exchange_type='fanout')
    channel.queue_declare(queue='MyQueue')
    channel.queue_declare(queue='MyQueue2')
    channel.queue_bind(exchange='MyExchange', queue='MyQueue')
    channel.queue_bind(exchange='MyExchange', queue='MyQueue2')


    for x in range(1000):      
        person = fake.name() 
        location = fake.city() 
        message = f"{person} is in city {location}"
        channel.basic_publish(exchange='MyExchange',routing_key='',body=message)


