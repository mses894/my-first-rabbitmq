import pika
from bidi.algorithm import get_display


def callback(ch, method, properties, body):
    print(f"Write to DB: " + get_display(body.decode('utf-8')))
    #print(f"Write to DB: {body.decode('utf-8')}")


connection_parameters = pika.ConnectionParameters(host='localhost')
with pika.BlockingConnection(connection_parameters) as connection:
    channel = connection.channel()
    channel.queue_declare(queue='MyQueue')
    channel.basic_consume(queue='MyQueue',on_message_callback=callback,auto_ack=True)
    channel.start_consuming()


   